(function() {
	var displayResults, findAll, maxResults, names, resultsOutput, searchInput;

	names = ["Gentry", "Brandi", "Maryann", "Tina", "Harper", "Hayes", "Tamera", "Shauna", "Mcfarland", "Charles", "Ortiz", "Maynard", "Julie", "Gay", "Wiggins", "Navarro", "Hopkins", "Candace", "Tammi", "Horton", "Erna", "Mills", "Opal", "Wolfe", "Walter", "Bonita", "Eleanor", "Rojas", "Ochoa", "Kirk", "Rosario", "Ball", "Lucile", "Kayla", "Carmela", "Miranda", "Middleton", "Lillie", "Sherry", "Jacqueline", "Deirdre", "Mueller", "Debra", "Jodi", "Joyce", "Estrada", "Liz", "Justine", "Francis", "Benton", "Henrietta", "Elise", "Lang", "Morse", "Farrell", "Tamra", "Darla", "Amy", "Kristie", "Wyatt", "Mcbride", "Talley", "Fay", "Sweet", "Fern", "Mcintosh", "Clemons", "Travis", "Kirsten", "Rios", "Newman", "Cook", "Jocelyn", "Mcmillan", "Mona", "Bessie", "Francis", "Rosemary", "Beverly", "Chandra", "Luella", "Parrish", "Ronda", "Earlene", "Bright", "Guthrie", "Shana", "Theresa", "Wells", "Green", "Schroeder", "Russo", "Randolph", "Livingston", "Carroll", "Velasquez", "Dana", "Bridget", "Hines", "Martha", "Marci", "Fuentes", "Stuart", "Glass", "Alejandra", "Thornton", "Britt", "Jeri", "Leach", "Cleo", "Lela", "Mattie", "Bonnie", "Lucille", "Mamie", "Kelly", "Obrien", "Carol", "Murphy", "Isabella", "Lowery", "Odom", "Norris", "Mullins", "Florine", "Morales", "Frederick", "Reynolds", "Janine", "Joyce", "Dean", "Marcy", "Allison", "Rena", "Saundra", "Flossie", "Kristi", "Monica", "Molina", "Guzman", "Loretta", "Levine", "Oneill", "Mccray", "Mann", "Constance", "English", "Eula", "Butler", "Erika"];

	// Match words in a collection
	findAll = (wordList, collection) => {
		return collection.filter(function(word) {
			word = word.toLowerCase();

			return wordList.some(function(w) {
				return ~word.indexOf(w);
			});
		});
	};

  // Displays results in a list
	displayResults = function(resultsEl, wordList) {
		return resultsEl.innerHTML = (wordList.map(function(w) {
			return '<li>' + w + '</li>';
		})).join('');
	};

	// Handle keyboard events
	searchInput = document.getElementById('search');

	resultsOutput = document.getElementById('results');

	maxResults = 7;

	searchInput.addEventListener('keyup', (e) => {
		var suggested, value;
		value = searchInput.value.toLowerCase().split(' ');
		suggested = (value[0].length ? findAll(value, names) : []);
		return displayResults(resultsOutput, suggested);
	});

}).call(this);